import React from 'react';

class BookStore extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentStep: 1 };
  }
  render() {
    switch (this.state.currentStep) {
      case 1:
        return <BookList />;
      case 2:
        return <ShippingDetails />;
      case 3:
        return <DeliveryDetails />;
      default:
        // do nothing
    }
  }
}

class BookList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      books: [
        { name: 'Zero to One', author: 'Peter Thiel' },
        { name: 'Monk who sold his Ferrari', author: 'Robin Sharma' },
        { name: 'Wings of Fire', author: 'A.P.J. Abdul Kalam' },
      ],
    };
  }
  _renderBook(book) {
    return (
      <div className="checkbox">
        <label>
          <input type="checkbox" /> {book.name} == {book.author}
        </label>
      </div>
    );
  }
  render() {
    return (
      <div>
        <h3>Choose fromwide varietyof books availablein our store</h3>
        <form onSubmit={this.handleSubmit}>
          { this.state.books.map((book) => { return (this._renderBook(book)) }) }
          <input type="submit" className="btn btn-success" />
        </form>
      </div>
    );
  }
  handleSubmit(event) {
    console.log(event);
    event.preventDefault();
    console.log("Form submitted");
  }
}

class ShippingDetails extends React.Component {
  render() {
    return (
      <h1>Enter your shipping informations</h1>
    );
  }
}

class DeliveryDetails extends React.Component {
  render() {
    return (
      <h1>Choose your delivery option here</h1>
    );
  }
}
