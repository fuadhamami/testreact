// Mengenal "properties" pada komponen React untuk komunikasi antar komponen yang berbeda.

import React from 'react';

const PropTypes = require('prop-types');

//Komponen untuk menampilkan warna
class ColorDisplay extends React.Component {
  render() {
    let displayStyle = { backgroundColor: '#' + this.props.bgcol };
    return (
      <h1 style={displayStyle}>Ganti warna latar!</h1>
    );
  }
}

//Komponen tombol dengan warna-warna preset
class PresetButton extends React.Component {
  handleClick(event) {
    this.props.onColorSelected(this.props.preset);
  }
  render() {
    let displayStyle = {backgroundColor: '#'+this.props.preset};
    return (
      <button onClick={this.handleClick}><i style={displayStyle}>&nbsp;&nbsp;&nbsp;</i> {this.props.preset}</button>
    );
  }
}

//Komponen untuk memasukkan nilai hexadesimal warna
class TextInput extends React.Component {
  handleTextInput(event) {
    this.props.onColorSelected(event.target.value);
  }
  render() {
    return (
      <input type="text" onChange={this.handleTextInput} value={this.props.currentColor}/>
    );
  }
}

TextInput.contextTypes = {
  currentColor: PropTypes.string,
};

export default class ColorDisplayApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'ccc',
    };
  }
  handleColorSelection(selectedColor) {
    this.setState({ color: selectedColor });
  }
  render() {
    return (
      <div>
        <ColorDisplay bgcol={this.state.color}/>
        <PresetButton preset="c00" onColorSelected={this.handleColorSelection}/>
        <PresetButton preset="0c0" onColorSelected={this.handleColorSelection}/>
        <PresetButton preset="00c" onColorSelected={this.handleColorSelection}/>
        <TextInput onColorSelected={this.handleColorSelection} currentColor={this.state.color}/>
      </div>
    );
  }
}
