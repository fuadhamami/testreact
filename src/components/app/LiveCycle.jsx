import React from 'react';

export default class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.tick = this.tick.bind(this);
    this.state = {
      secElapsed: 0,
    };
  }
  componentDidMount() {
    this.interval = setInterval(this.tick, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  tick() {
    this.setState({ secElapsed: this.state.secElapsed + 1 });
  }
  render() {
    return (
      <div>
        <p>Pengenalan <em>life-cycle</em> komponen React.</p>
        <div>Detik terlewati: {this.state.secElapsed}</div>
      </div>
    );
  }
}
